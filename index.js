const colors = [
    'rgba(200,50,0,.7)',
    'rgba(0,0,0,.7)',
    'rgba(50,70,150,.7)',
    'rgba(78,52,46,.8)',
    'rgba(100,0,0,.7)'
]

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems);
  });


// create a wrapper around native canvas element (with id="c")
var canvas = new fabric.Canvas('c');
canvas.preserveObjectStacking = true;
fabric.Object.prototype.objectCaching = false;

var canvasWrapper = document.getElementById('canvaswrapper');
canvasWrapper.tabIndex = 1000;

var circle = new fabric.Circle({
    radius: 1500, fill: 'rgba(200,50,0,.7)', left: -1500, top: 350
});
canvas.add(circle);
circle.moveTo(1);


let image;
let logo;
let texts = [
    new fabric.Text('Titel', { left: 30, top: 430, fontSize: 60, fontFamily: 'Roboto Slab', fill: 'white', shadow: 'rgba(0,0,0,0.3) 5px 5px 5px' }),
    new fabric.Text('Untertitel', { left: 30, top: 500, fontSize: 20, fontFamily: 'Roboto Slab', fill: 'white', shadow: 'rgba(0,0,0,0.3) 5px 5px 5px' }),
    new fabric.Text('Datum', { left: 30, top: 400, fontSize: 20, fontFamily: 'Roboto Slab', fill: 'white', shadow: 'rgba(0,0,0,0.3) 5px 5px 5px' }),
]

texts.forEach((text) => {
    canvas.add(text)
})

fabric.Image.fromURL('img.jpg', function (img) {

    // add filter
    //img.filters.push(new fabric.Image.filters.Grayscale());
    image = img;

    image.filters.push(new fabric.Image.filters.Saturation());
    image.filters.push(new fabric.Image.filters.Brightness());
    image.filters.push(new fabric.Image.filters.Contrast());
    image.filters.push(new fabric.Image.filters.Blur());

    // apply filters and re-render canvas when done
    image.applyFilters();
    // add image onto canvas (it also re-render the canvas)
    canvas.add(image);
    image.moveTo(0)
    //console.log(canvas.toDataURL());

});

fabric.Image.fromURL('sfn-logo.png', function (img) {

    logo = img;
    logo.set({
        left: 20,
        top: 20,
        shadow: 'rgba(0,0,0,0.3) 5px 5px 5px' 
    });
    logo.scaleToWidth(100)
    canvas.add(logo);
    logo.moveTo(1)
    

});

// Code for moving objects with arrow keys


var Direction = {
    LEFT: 0,
    UP: 1,
    RIGHT: 2,
    DOWN: 3
};

fabric.util.addListener(canvasWrapper, 'keydown', function (options) {
    if (options.repeat) {
        return;
    }
    var key = options.which || options.keyCode; // key detection
    if (key === 37) { // handle Left key
        moveSelected(Direction.LEFT);
    } else if (key === 38) { // handle Up key
        moveSelected(Direction.UP);
    } else if (key === 39) { // handle Right key
        moveSelected(Direction.RIGHT);
    } else if (key === 40) { // handle Down key
        moveSelected(Direction.DOWN);
    }
});


const STEP = 5;


function moveSelected(direction) {

    var activeObject = canvas.getActiveObject();
    var activeGroup = null;

    console.log(activeObject)

    if (activeObject) {
        switch (direction) {
        case Direction.LEFT:
            activeObject.set('left', activeObject.left - STEP);
            break;
        case Direction.UP:
            activeObject.set('top', activeObject.top - STEP);
            break;
        case Direction.RIGHT:
            activeObject.set('left', activeObject.left + STEP);
            break;
        case Direction.DOWN:
            activeObject.set('top', activeObject.top + STEP);
            break;
        }
        activeObject.setCoords();
        canvas.renderAll();
        console.log('selected objects was moved');
    } else {
        console.log('no object selected');
    }

}

function toImage() {
    var img = document.getElementById("exportimg");
    img.src = canvas.toDataURL({ multiplier: 2, format: 'jpeg' });
}



function renderImage() {
    image.applyFilters();
    canvas.renderAll();
}

function setSaturation(val) {
    image.filters[0].saturation = val;
    renderImage();
}

function setBrightness(val) {
    image.filters[1].brightness = val;
    renderImage();
}

function setContrast(val) {
    image.filters[2].contrast = val;
    renderImage();
}

function setBlur(val) {
    image.filters[3].blur = val;
    renderImage();
}

function setText(i, text) {
    texts[i].text = text;
    canvas.renderAll();
}

function setColor(i, o) {
    if(!isNaN(i)){
        circle.fill = colors[i]
    } else {
        const c = new fabric.Color(i);
        c.setAlpha(0.7)
        circle.fill = c.toRgba();
    }

    
    
    canvas.renderAll();
}

function setLogo(val){
    logo.opacity = (val) ? 1 : 0;
    canvas.renderAll();
}


function setLogoColor(c)  {
    var filter = new fabric.Image.filters.BlendColor({
        color: c,
        mode: 'multiply'
       });
    
    logo.filters = [filter]
    logo.applyFilters();
    canvas.renderAll();
}

document.getElementById('imgLoader').onchange = function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) {
        canvas.remove(image);
        var imgObj = new Image();
        imgObj.src = event.target.result;
        imgObj.onload = function () {
            image = new fabric.Image(imgObj);
            let imageTextureSize = image.width > image.height ? image.width : image.height;

            if (imageTextureSize > fabric.textureSize) {
                    fabric.textureSize = imageTextureSize;
            }
            canvas.add(image);
            image.moveTo(0);
            if (image.width > image.height){
                image.scaleToHeight(540);
            } else {
                image.scaleToWidth(540);
            }
            
            image.filters.push(new fabric.Image.filters.Saturation());
            image.filters.push(new fabric.Image.filters.Brightness());
            image.filters.push(new fabric.Image.filters.Contrast());
            image.filters.push(new fabric.Image.filters.Blur());

            canvas.centerObject(image);
            canvas.renderAll();
        }
    }
    reader.readAsDataURL(e.target.files[0]);
}


// Color picker

const colorpicker = document.getElementById('selected_bg_color')
const logocolorpicker = document.getElementById('selected_logo_color')

colorpicker.onchange = () => {
    const c = colorpicker.value
    console.log(c)
    colorpicker.style.backgroundColor = c;
    setColor(c)
}

logocolorpicker.onchange = () => {
    const c = logocolorpicker.value
    console.log(c)
    logocolorpicker.style.backgroundColor = c;
    setLogoColor(c)
}